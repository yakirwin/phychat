PhyChat - a GPS based app to chat with strangers. In person.
===================

The app uses the user GPS based phone to find him a stranger nearby to meet and chat on any topic. The idea is to help people find conversation partners or just study buddies. Users can specify a specific topic they want to talk about and other nearby users will see their request. The app could be used to talk about topics like science, philosophy, religion or just to find a partner to study to a nearby test. Elders could use the app to find new people to speak with if they don't any.
